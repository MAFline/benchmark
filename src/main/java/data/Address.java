package data;

/**
 * This class represent Address.
 * 
 * @author Lukáš Masařík
 * 
 */
public class Address {
	private String id;
	private String city;
	private String street;
	private int houseNumber;

	public Address() {

	}

	public Address(String id, String city, String street, int houseNumber) {
		this.id = id;
		this.city = city;
		this.street = street;
		this.houseNumber = houseNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	@Override
	public String toString() {
		String address = city + ", " + street + " " + houseNumber;
		return address;
	}

}
