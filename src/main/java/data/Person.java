package data;

/**
 * This class represent Person.
 * 
 * @author Lukáš Masařík
 * 
 */
public class Person {
	private String id;
	private String firstName;
	private String lastName;
	private int age;
	private int salary;
	private Address address;

	public Person() {

	}

	public Person(String id, String firtName, String lastName, int age,
			int salary, Address address) {
		this.id = id;
		this.firstName = firtName;
		this.lastName = lastName;
		this.age = age;
		this.salary = salary;
		this.address = address;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		String person = firstName + " " + lastName + ", " + age + " let, "
				+ salary + ",--, " + address.toString();
		return person;
	}

}
