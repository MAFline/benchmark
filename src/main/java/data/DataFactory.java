package data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Factory to generate random data.
 * 
 * @author Lukáš Masařík
 * 
 */
public class DataFactory {

	/**
	 * Generate list of persons.
	 * 
	 * @param count
	 *            - number of persons to generate.
	 * @return - List of Persons (ArrayList).
	 */
	public static List<Person> generatePersons(int count) {
		List<Person> persons = new ArrayList<>();
		List<String> names = Arrays.asList("Kvído", "Arpád", "Veleslav",
				"Velen", "Gedeon", "Rupert", "Heřman", "Armin", "Ratmír",
				"Julius", "Zenon", "Vincenc", "Oktavián", "Ireneus", "Valér");
		List<String> first = Arrays.asList("Ko", "Va", "He", "Ma", "Svo", "Pe",
				"Le", "Ro", "Fo", "Pan", "Gla", "Dob", "Ši", "Te", "Pi",
				"Mali", "Sva", "No", "Ne", "Ze", "Novo");
		List<String> middle = Arrays.asList("va", "sař", "tů", "bo", "lec",
				"li", "lo", "ni", "bro", "vol", "krá", "metu", "sic", "ber",
				"fá", "lec", "po", "sa", "žič", "bí", "no");
		List<String> last = Arrays.asList("ský", "na", "do", "ný", "vec", "ra",
				"ček", "da", "nek", "er", "vka", "pal", "mec", "řík", "věd",
				"ník", "ov", "vák", "dal", "šek", "ký");
		Random rnd = new Random();

		for (int i = 0; i < count; i++) {
			String id = UUID.randomUUID().toString();
			String firstName = names.get(rnd.nextInt(names.size()));
			String lastName = first.get(rnd.nextInt(first.size()))
					+ middle.get(rnd.nextInt(middle.size()))
					+ last.get(rnd.nextInt(last.size()));
			int age = rnd.nextInt(50) + 20;
			int salary = (rnd.nextInt(60) + 10) * 1000;
			Address address = generateAddresses(1).get(0);
			persons.add(new Person(id, firstName, lastName, age, salary,
					address));
		}

		return persons;
	}

	/**
	 * Generate list of addresses.
	 * 
	 * @param count
	 *            - number of addresses to generate.
	 * @return - List of Addresses (ArrayList).
	 */
	public static List<Address> generateAddresses(int count) {
		List<Address> addresses = new ArrayList<>();
		List<String> cities = Arrays.asList("Praha", "Brno", "Hradec Králové",
				"Pardubice", "Frýdek-Místek", "Ostrava");
		List<String> streets = Arrays.asList("Pražská", "Brněnská",
				"Královehradecká", "Pardubická", "Frýdecká", "Místecká",
				"Ostravská", "Lužná", "Stodolní", "Heyrovského",
				"Václavské Náměstí", "Schweigerova", "Podgorného", "Hostýnská",
				"Plotní", "Trnitá", "Klášterského", "Tuháčkova");
		Random rnd = new Random();

		for (int i = 0; i < count; i++) {
			String id = UUID.randomUUID().toString();
			String city = cities.get(rnd.nextInt(cities.size()));
			String street = streets.get(rnd.nextInt(streets.size()));
			int houseNumber = rnd.nextInt(99) + 1;
			addresses.add(new Address(id, city, street, houseNumber));
		}

		return addresses;
	}

}
