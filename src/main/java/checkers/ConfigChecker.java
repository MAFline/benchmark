package checkers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import test.suite.SuiteFactory;
import test.suite.TestSuite;
import bindings.SuitesBinding;
import bindings.SuitesBinding.Suite;
import bindings.SuitesBinding.Suite.Test;

/**
 * Class to process configuration file and create TestSuites.
 * 
 * @author Lukáš Masařík
 * 
 */
public class ConfigChecker {
	private final String CONFIG_FILE = "/default_config.json";
	private List<TestSuite<?, ?>> suites;

	public ConfigChecker() {

	}

	/**
	 * Initialize configuration with default file.
	 */
	public void init() {
		try {
			parseJsonConfiguration(this.getClass().getResourceAsStream(CONFIG_FILE));
		} catch (IOException e) {
		}
	}

	/**
	 * Initialize configuration file.
	 * 
	 * @param path
	 */
	public void init(String path) {
			System.out.println("Parsovani souboru " + path.toString());
			try {
				parseJsonConfiguration(new FileInputStream(path));
			} catch (IOException e) {
			}
	}

	/**
	 * Parse JSON document and if it's possible, create Arraylist of TestSuites.
	 * 
	 * @param path
	 *            Path to Json file.
	 */
	private void parseJsonConfiguration(InputStream path) throws IOException {
		suites = new ArrayList<>();
		
		ObjectMapper mapper = new ObjectMapper();
		SuitesBinding suites = mapper.readValue(path ,
				SuitesBinding.class);
		for (Suite suite : suites.getSuites()) {
			TestSuite<?, ?> testSuite = SuiteFactory.createTestSuite(suite);

			if (testSuite != null) {
				for (Test test : suite.getTests()) {
					test.Test newTest = SuiteFactory.createTest(test,
							suite.getSuiteName());
					if (newTest != null) {
						testSuite.addTest(newTest);
					}
				}
				this.suites.add(testSuite);
			}
		}
	}

	public List<TestSuite<?, ?>> getSuites() {
		return suites;
	}

	public void setSuites(List<TestSuite<?, ?>> suites) {
		this.suites = suites;
	}

}
