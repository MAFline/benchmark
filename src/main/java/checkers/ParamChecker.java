package checkers;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class to check parameters from command line.
 * 
 * @author Lukáš Masařík
 * 
 */
public class ParamChecker {
	private boolean help;
	private boolean jsonFile;

	public ParamChecker() {
		help = false;
		jsonFile = false;
	}

	/**
	 * Analyze input parameter if it's help option, JSON object with
	 * configuration or anything.
	 * 
	 * @param param
	 *            input param
	 */
	public void check(String param) {
		if (param.equals("-h")) {
			help = true;
			return;
		}

		if (isValidJsonFile(param)) {
			jsonFile = true;
		}
	}

	/**
	 * Check if file is valid JSON file.
	 * 
	 * @param file
	 *            path to file.
	 * @return true, if file exists and has json extension.
	 */
	private boolean isValidJsonFile(String file) {
		Path path = Paths.get(file).toAbsolutePath().normalize();

		if (!Files.exists(path, LinkOption.NOFOLLOW_LINKS)
				|| !path.toString().toLowerCase().endsWith(".json")) {
			return false;
		}
		return true;
	}

	public boolean isHelp() {
		return help;
	}

	public void setHelp(boolean help) {
		this.help = help;
	}

	public boolean isJsonFile() {
		return jsonFile;
	}

	public void setJsonFile(boolean jsonFile) {
		this.jsonFile = jsonFile;
	}

}
