package bindings;

import java.util.List;

/**
 * This class is for handling configuration file.
 * 
 * @author Lukáš Masařík
 * 
 */
public class SuitesBinding {

	public static class Suite {

		public static class Test {
			private String packageName;
			private String testName;
			private int count;

			public String getPackageName() {
				return packageName;
			}

			public void setPackageName(String packageName) {
				this.packageName = packageName;
			}

			public String getTestName() {
				return testName;
			}

			public void setTestName(String testName) {
				this.testName = testName;
			}

			public int getCount() {
				return count;
			}

			public void setCount(int count) {
				this.count = count;
			}

		}

		private String suiteName, dbserver, dbname, username, password;
		private int dbport;
		private List<Test> tests;

		public String getSuiteName() {
			return suiteName;
		}

		public void setSuiteName(String suiteName) {
			this.suiteName = suiteName;
		}

		public String getDbserver() {
			return dbserver;
		}

		public void setDbserver(String dbserver) {
			this.dbserver = dbserver;
		}

		public String getDbname() {
			return dbname;
		}

		public void setDbname(String dbname) {
			this.dbname = dbname;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public int getDbport() {
			return dbport;
		}

		public void setDbport(int dbport) {
			this.dbport = dbport;
		}

		public List<Test> getTests() {
			return tests;
		}

		public void setTests(List<Test> tests) {
			this.tests = tests;
		}

	}

	private List<Suite> suites;

	public List<Suite> getSuites() {
		return suites;
	}

	public void setSuites(List<Suite> suites) {
		this.suites = suites;
	}

}
