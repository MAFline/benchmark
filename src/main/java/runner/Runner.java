package runner;

/**
 * Interface to manage application run.
 * 
 * @author Lukáš Masařík
 * 
 */
public interface Runner {

	/**
	 * Initialize application.
	 */
	public void init();

	/**
	 * Run application.
	 */
	public void run();
}
