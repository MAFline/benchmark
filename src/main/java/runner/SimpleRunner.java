package runner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import test.suite.TestSuite;
import checkers.ConfigChecker;
import checkers.ParamChecker;

/**
 * Runner to managed application through command line.
 * 
 * @author Lukáš Masařík
 * 
 */
public class SimpleRunner implements Runner {
	private String[] params;
	private ParamChecker paramChecker;
	private ConfigChecker config;
	private final String CONFIG_EXAMPLE = "/config_example.json";
	private final String TESTS = "/tests.properties";

	public SimpleRunner(String[] params) {
		this.params = params;
		this.paramChecker = new ParamChecker();
		this.config = new ConfigChecker();
	}

	@Override
	public void init() {

		if (params.length > 0) {
			paramChecker.check(params[0]);

			if (paramChecker.isHelp()) {
				printHelp();
				System.out.println("Konec programu.");
				System.exit(0);
			} else if (paramChecker.isJsonFile()) {
				config.init(params[0]);
				return;
			}

			System.out
					.println("Zadany parametr neodpovida JSON souboru ani volitelnym parametrum.");

		} else {
			System.out.println("Nevybran zdrojovy konfiguracni soubor.");
		}

		useDefaultConfig();

	}

	@Override
	public void run() {
		List<TestSuite<?, ?>> suites = config.getSuites();
		if (!suites.isEmpty()) {
			for (TestSuite<?, ?> testSuite : suites) {
				if (!testSuite.getTests().isEmpty()) {
					testSuite.init();
					testSuite.run();
					testSuite.tearDown();
				} else {
					System.out.println(testSuite.getClass().getSimpleName()
							+ " neobsahuje zadne testy.");
				}
			}
		} else {
			System.out
					.println("Nebylo možné načíst žádné testy z konfiguračního dokumentu.");
		}

		System.out.println("Konec programu.");
	}

	/**
	 * Print help describing how to configure JSON configuration file with
	 * tests.
	 * 
	 * @throws InterruptedException
	 */
	private void printHelp() {
		System.out
				.println("Srovnavaci benchmark databazi MongoDB, MySQL, Derby.");

		System.out
				.println("Program vyuziva jako konfiguracni soubor testu JSON dokument. \n");

		System.out.println("Pouzitelne parametry: \n");
		System.out.println("\t -h \t\t zobrazi help.");
		System.out
				.println("\t [path] \t cesta souboru s JSON objektem s nastavenou konfiguraci. \n");
		System.out
				.println("Nelze vyuzit parametry soucasne. V potaz bude bran pouze prvni. \n");

		System.out.println("Struktura konfiguracniho JSON dokumentu: \n");

		InputStream configExample = this.getClass().getResourceAsStream(
				CONFIG_EXAMPLE);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				configExample))) {
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
		}

		System.out.printf("%n");
		System.out.println("V defaultni konfiguraci jsou veskere dostupne "
				+ "testy s poctem 20000. Veskere databaze vyuzivaji "
				+ "localhost s defaultnim portem pro databazi. U MySQL "
				+ "je pouzit uzivatel 'root' bez hesla.");
		System.out.printf("%n");
		printTestClasses();
	}

	/**
	 * Print all possible database suites with tests.
	 */
	private void printTestClasses() {
		System.out
				.println("Do konfiguracniho souboru je mozne vlozit nasledujici databaze a testy:\n");
		System.out.println("suiteName");
		System.out.println("\tpackageName");
		System.out.println("\t\ttestName\n");

		InputStream configExample = this.getClass().getResourceAsStream(TESTS);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				configExample))) {
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
		}
	}

	/**
	 * If program parameters are invalid, print help message and ask to continue
	 * with default configuration.
	 */
	private void useDefaultConfig() {

		System.out
				.println("Pro moznosti nacteni konfiguracniho souboru spustte program s parametrem -h.");
		System.out
				.print("Prejete si spustit benchmark s defaultni konfiguraci [d], nápovědu [h] nebo ukončit program [k]? [D/h/k]: ");

		String confirm = null;

		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				System.in))) {
			confirm = br.readLine().toLowerCase();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (confirm.equals("d") || confirm.equals("")) {
			config.init();
			return;
		} else if (confirm.equals("h")) {
			printHelp();
		}

		System.out.println("Konec programu.");
		System.exit(0);
	}

}
