package main;

import runner.Runner;
import runner.SimpleRunner;

/**
 * Enter point of the application.
 * 
 * @author Lukáš Masařík
 * 
 */
public class MainApp {

	public static void main(String[] args) {

		Runner runner = new SimpleRunner(args);
		runner.init();
		runner.run();

	}
}
