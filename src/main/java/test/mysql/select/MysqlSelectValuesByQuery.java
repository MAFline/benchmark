package test.mysql.select;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.mysql.MysqlTest;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetImpl;
import com.mysql.jdbc.ResultSetInternalMethods;

public class MysqlSelectValuesByQuery extends MysqlTest {
	private PreparedStatement psSelect;
	private Logger logger;

	public MysqlSelectValuesByQuery(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(MysqlSelectValuesByQuery.class);
	}

	@Override
	public void init() {
		insertPersons(numOfRecords, false);

		String sqlSelect = "SELECT firstName, lastName, salary, city FROM "
				+ tables.get("persons")
				+ " JOIN "
				+ tables.get("addresses")
				+ " using (id_address)"
				+ " WHERE city = 'Brno' AND salary >= 40000";
		
		try {
			psSelect = (PreparedStatement) connection
					.prepareStatement(sqlSelect);
		} catch (SQLException e) {
			logger.error("Nepodarilo se iniciovat MySQL prikaz pro cteni.");
		}
	}

	@Override
	public void run() {
		try (ResultSetInternalMethods rs = (ResultSetImpl) psSelect.executeQuery()) {
			psSelect.close();
		} catch (SQLException e) {
			logger.error("Cteni se nezdarilo");
		}
	}
	
	@Override
	public void tearDown() {
		String sql = "DROP TABLE ";
		
		try (PreparedStatement persons = (PreparedStatement) connection.prepareStatement(sql
				+ tables.get("persons"));
				PreparedStatement addresses = (PreparedStatement) connection.prepareStatement(sql
						+ tables.get("addresses"))) {
			
			persons.executeUpdate();
			addresses.executeUpdate();
			
			tables.remove("persons");
			tables.remove("addresses");
		} catch (SQLException e) {
			logger.error("Nelze smazat tabulky po ukonceni testu.");
		}
	}
}
