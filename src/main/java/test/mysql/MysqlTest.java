package test.mysql;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import data.DataFactory;
import data.Person;

/**
 * Class implements interface Test and collect common parameters and methods for
 * MySQL tests.
 * 
 * @author Lukáš Masařík
 *
 */
public abstract class MysqlTest implements Test {
	protected Connection connection;
	protected int numOfRecords;
	protected Map<String, String> tables;
	private DateFormat df;
	private Logger logger;

	public MysqlTest(int numOfRecords) {
		this.numOfRecords = numOfRecords;
		df = new SimpleDateFormat("ddMMyyyy_HHmmss");
		tables = new HashMap<>();
		logger = LoggerFactory.getLogger(MysqlTest.class);
	}

	/**
	 * Create MySQL table with MyISAM engine, add suffix with class and actual
	 * date.
	 * 
	 * @param name
	 *            Table name.
	 */
	protected void createTable(String name) {
		String tableName = name
				+ "_"
				+ this.getClass().getSimpleName().toLowerCase()
						.replace(".", "_") + "_" + df.format(new Date());

		String sqlCreateTable = "CREATE TABLE " + tableName
				+ getTableColumns(name);

		try (PreparedStatement createTable = (PreparedStatement) connection
				.prepareStatement(sqlCreateTable);) {

			createTable.executeUpdate();
			tables.put(name, tableName);
			Thread.sleep(1000);
		} catch (SQLException e) {
			logger.error("Nepodarilo se vytvorit tabulku '{}'.", tableName, e);
		} catch (InterruptedException e) {
			logger.error("Chyba uspani vlakna.");
		}
	}

	/**
	 * Generate String to CREATE TABLE command.
	 * 
	 * @param table
	 *            Table which will be created. Possible values: addresses,
	 *            persons.
	 * @return String with parameters to create new table.
	 */
	private String getTableColumns(String table) {
		String sqlColumns = new String();
		switch (table) {
		case "addresses":
			sqlColumns = " (id_address VARCHAR(50) NOT NULL, street VARCHAR(50), houseNumber INTEGER, city VARCHAR(50), PRIMARY KEY (id_address))";
			break;
		case "persons":
			sqlColumns = " (id_person VARCHAR(50) NOT NULL, firstName VARCHAR(20), lastName VARCHAR(50), age INTEGER, salary INTEGER, id_address VARCHAR(50), PRIMARY KEY (id_person))";
			break;
		default:
			break;
		}
		return sqlColumns;
	}

	/**
	 * Set foreign key constraint between two tables.
	 * 
	 * @param table
	 *            table name where you put foreign key constraint.
	 * @param column
	 *            column which references to other table.
	 * @param referencedTable
	 *            table name where foreign key points.
	 * @param referencedColumn
	 *            column on referenced table.
	 */
	protected void setForeignKey(String table, String column,
			String referencedTable, String referencedColumn) {
		String sqlForeignKey = "ALTER TABLE " + tables.get(table)
				+ " ADD FOREIGN KEY (" + column + ") REFERENCES "
				+ tables.get(referencedTable) + "(" + referencedColumn + ")";

		try (PreparedStatement foreignKey = (PreparedStatement) connection
				.prepareStatement(sqlForeignKey)) {
			foreignKey.executeUpdate();
		} catch (SQLException e) {
			logger.error(
					"Nepodarilo se vytvorit cizi klic mezi tabulkami '{}' a '{}'.",
					tables.get(table), tables.get(referencedTable));
		}
	}

	/**
	 * Create index on selected table and column.
	 * 
	 * @param table
	 *            Table where to create index.
	 * @param column
	 *            Column where ensure index.
	 */
	protected void createIndex(String table, String column) {
		String sqlCreateIndex = "CREATE INDEX index_" + column + " ON "
				+ tables.get(table) + " (" + column + ")";

		try (PreparedStatement createIndex = (PreparedStatement) connection
				.prepareStatement(sqlCreateIndex)) {
			createIndex.executeUpdate();
		} catch (SQLException e) {
			logger.error(
					"Nepodarilo se vytvorit index na sloupci '{}' tabulky '{}'.",
					column, tables.get(table));
		}
	}

	/**
	 * Insert List of persons to the database. The method will create two tables
	 * - Addresses and Persons. Then will insert the list.
	 * 
	 * @param count
	 *            - number of records to insert.
	 * @param index
	 *            - create table with or without index.
	 */
	protected void insertPersons(int count, boolean index) {
		createTable("persons");
		createTable("addresses");
		setForeignKey("persons", "id_address", "addresses", "id_address");

		String sqlInsertAddress = "INSERT INTO "
				+ tables.get("addresses")
				+ " (id_address, street, houseNumber, city) VALUES (?, ?, ?, ?)";
		String sqlInsertPerson = "INSERT INTO "
				+ tables.get("persons")
				+ " (id_person, firstName, lastName, age, salary, id_address) VALUES (?, ?, ?, ?, ?, ?)";

		List<Person> persons = DataFactory.generatePersons(numOfRecords);

		try (PreparedStatement psAddress = (PreparedStatement) connection
				.prepareStatement(sqlInsertAddress);
				PreparedStatement psPerson = (PreparedStatement) connection
						.prepareStatement(sqlInsertPerson)) {

			for (Person person : persons) {
				psAddress.setString(1, person.getAddress().getId());
				psAddress.setString(2, person.getAddress().getStreet());
				psAddress.setInt(3, person.getAddress().getHouseNumber());
				psAddress.setString(4, person.getAddress().getCity());

				psAddress.addBatch();

				psPerson.setString(1, person.getId());
				psPerson.setString(2, person.getFirstName());
				psPerson.setString(3, person.getLastName());
				psPerson.setInt(4, person.getAge());
				psPerson.setInt(5, person.getSalary());
				psPerson.setString(6, person.getAddress().getId());

				psPerson.addBatch();
			}

			psAddress.executeBatch();
			psPerson.executeBatch();

			if (index) {
				createIndex("persons", "salary");
				createIndex("persons", "lastName");
				createIndex("addresses", "city");
			}
		} catch (SQLException e) {
			logger.error("Nepodarilo se zapsat data do databaze.");
			return;
		}
	}

	@Override
	public void setConnection(Object connection) {
		if (connection == null) {
			throw new RuntimeException(
					"Nelze komunikovat s databází (nenalezen objekt ke komunikaci)");
		}
		this.connection = (Connection) connection;
	}

	@Override
	public int getNumOfRecords() {
		return numOfRecords;
	}

}
