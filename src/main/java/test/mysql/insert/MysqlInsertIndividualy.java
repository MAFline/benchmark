package test.mysql.insert;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.mysql.MysqlTest;

import com.mysql.jdbc.PreparedStatement;

import data.DataFactory;
import data.Person;

public class MysqlInsertIndividualy extends MysqlTest {
	private List<Person> persons;
	private PreparedStatement psAddress;
	private PreparedStatement psPerson;
	private Logger logger;

	public MysqlInsertIndividualy(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(MysqlInsertIndividualy.class);
	}

	@Override
	public void init() {
		createTable("persons");
		createTable("addresses");
		setForeignKey("persons", "id_address", "addresses", "id_address");

		String sqlInsertAddress = "INSERT INTO "
				+ tables.get("addresses")
				+ " (id_address, street, houseNumber, city) VALUES (?, ?, ?, ?)";
		String sqlInsertPerson = "INSERT INTO "
				+ tables.get("persons")
				+ " (id_person, firstName, lastName, age, salary, id_address) VALUES (?, ?, ?, ?, ?, ?)";

		persons = DataFactory.generatePersons(numOfRecords);

		try {
			psAddress = (PreparedStatement) connection
					.prepareStatement(sqlInsertAddress);

			psPerson = (PreparedStatement) connection
					.prepareStatement(sqlInsertPerson);
			
		} catch (SQLException e) {
			logger.error("Nepodarilo se iniciovat MySQL prikazy pro zapis.");
		}
	}

	@Override
	public void run() {
		try {
			for (Person person : persons) {
				psAddress.setString(1, person.getAddress().getId());
				psAddress.setString(2, person.getAddress().getStreet());
				psAddress.setInt(3, person.getAddress().getHouseNumber());
				psAddress.setString(4, person.getAddress().getCity());

				psAddress.executeUpdate();

				psPerson.setString(1, person.getId());
				psPerson.setString(2, person.getFirstName());
				psPerson.setString(3, person.getLastName());
				psPerson.setInt(4, person.getAge());
				psPerson.setInt(5, person.getSalary());
				psPerson.setString(6, person.getAddress().getId());

				psPerson.executeUpdate();
			}
			psAddress.close();
			psPerson.close();
		} catch (SQLException e) {
			logger.error("Zapis se nezdaril");
		}
	}
	
	@Override
	public void tearDown() {
		String sql = "DROP TABLE ";
		
		try (PreparedStatement persons = (PreparedStatement) connection.prepareStatement(sql
				+ tables.get("persons"));
				PreparedStatement addresses = (PreparedStatement) connection.prepareStatement(sql
						+ tables.get("addresses"))) {
			
			persons.executeUpdate();
			addresses.executeUpdate();
			
			tables.remove("persons");
			tables.remove("addresses");
		} catch (SQLException e) {
			logger.error("Nelze smazat tabulky po ukonceni testu.");
		}
	}

}
