package test.mysql.insert;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.mysql.MysqlTest;

import com.mysql.jdbc.PreparedStatement;

import data.Address;
import data.DataFactory;

public class MysqlInsertIndividualyAddresses extends MysqlTest {
	private List<Address> addresses;
	private PreparedStatement psAddress;
	private Logger logger;

	public MysqlInsertIndividualyAddresses(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(MysqlInsertIndividualyAddresses.class);
	}

	@Override
	public void init() {
		createTable("addresses");

		String sqlInsertAddress = "INSERT INTO "
				+ tables.get("addresses")
				+ " (id_address, street, houseNumber, city) VALUES (?, ?, ?, ?)";

		addresses = DataFactory.generateAddresses(numOfRecords);

		try {
			psAddress = (PreparedStatement) connection
					.prepareStatement(sqlInsertAddress);
		} catch (SQLException e) {
			logger.error("Nepodarilo se iniciovat MySQL prikaz pro zapis.");
		}
	}

	@Override
	public void run() {
		try {
			for (Address a : addresses) {
				psAddress.setString(1, a.getId());
				psAddress.setString(2, a.getStreet());
				psAddress.setInt(3, a.getHouseNumber());
				psAddress.setString(4, a.getCity());

				psAddress.executeUpdate();
			}
			psAddress.close();
		} catch (SQLException e) {
			logger.error("Zapis se nezdaril");
		}
	}

	@Override
	public void tearDown() {
		String sql = "DROP TABLE ";

		try (PreparedStatement addresses = (PreparedStatement) connection
				.prepareStatement(sql + tables.get("addresses"))) {

			addresses.executeUpdate();
			
			tables.remove("addresses");
		} catch (SQLException e) {
			logger.error("Nelze smazat tabulky po ukonceni testu.");
		}
	}

}
