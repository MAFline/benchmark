package test;

/**
 * Interface defining basic test methods.
 * 
 * @author Lukáš Masařík
 * 
 */
public interface Test {

	/**
	 * Set parameters before running.
	 */
	public void init();

	/**
	 * Run test.
	 */
	public void run();

	/**
	 * Set object to communicate with database.
	 * 
	 * @param connection
	 *            Object communicating with database.
	 */
	public void setConnection(Object connection);

	/**
	 * Clear database space after test.
	 */
	public void tearDown();

	/**
	 * Return number of records for test.
	 */
	public int getNumOfRecords();
}
