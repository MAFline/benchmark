package test.suite;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;
import bindings.SuitesBinding.Suite;

/**
 * Factory to create TestSuites and Test from POJO objects with using static
 * methods.
 * 
 * @author Lukáš Masařík
 * 
 */
public class SuiteFactory {
	private static final String TEST_PACKAGE = "test";
	private static Logger logger = LoggerFactory.getLogger(SuiteFactory.class);

	/**
	 * Creating TestSuite from POJO object.
	 * 
	 * @param suite
	 *            POJO object with properties to create TestSuite.
	 * @return new TestSuite.
	 */
	public static TestSuite<? extends Test, ?> createTestSuite(Suite suite) {
		String error = "Nepodařilo se sestrojit TestSuite z konfiguračního dokumentu.";

		if (suite.getSuiteName() == null) {
			logger.error("Nezadan nazev suitu.");
			logger.error(error);
			return null;
		}

		if (suite.getDbserver() == null) {
			logger.error("Nezadana adresa databazoveho serveru pro suite.");
			logger.error(error);
			return null;
		}

		if (suite.getDbport() == 0) {
			logger.error("Nezadan port pro suite.");
			logger.error(error);
			return null;
		}

		if (suite.getDbname() == null) {
			logger.error("Nezadan nazev databaze pro suite.");
			logger.error(error);
			return null;
		}

		switch (suite.getSuiteName()) {
		case "mongo":
			return new MongoTestSuite(suite.getDbserver(), suite.getDbname(),
					suite.getDbport());
		case "mysql":
			String username = "",
			password = "";

			if (suite.getUsername() != null) {
				username = suite.getUsername();
			}

			if (suite.getPassword() != null) {
				password = suite.getPassword();
			}

			return new MysqlTestSuite(suite.getDbserver(), suite.getDbname(),
					suite.getDbport(), username, password);
		case "derby":
			return new DerbyTestSuite(suite.getDbserver(), suite.getDbname(),
					suite.getDbport());
		default:
			logger.error("Neznamy nazev pro suite.");
			logger.error(error);
			return null;
		}
	}

	/**
	 * Creating Test from POJO object.
	 * 
	 * @param test
	 *            POJO object with properties to create Test
	 * @param suiteName
	 *            database name (mongo, derby, mysql)
	 * @return new Test
	 */
	public static Test createTest(bindings.SuitesBinding.Suite.Test test,
			String suiteName) {

		Test t = null;

		if (test.getPackageName() == null) {
			logger.error("Nezadan nazev balicku pro test.");
			return t;
		}

		if (test.getTestName() == null) {
			logger.error("Nezadan nazev testu.");
			return t;
		}

		if (test.getCount() == 0) {
			logger.error("Nezadan pocet zaznamu pro test.");
			return t;
		}

		StringBuilder className = new StringBuilder(TEST_PACKAGE).append(".")
				.append(suiteName.toLowerCase()).append(".")
				.append(test.getPackageName().toLowerCase()).append(".")
				.append(test.getTestName());

		try {
			Class cl = Class.forName(className.toString());
			Constructor con = cl.getConstructor(Integer.TYPE);
			t = (Test) con.newInstance(test.getCount());
		} catch (ClassNotFoundException | NoSuchMethodException
				| SecurityException | InstantiationException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			logger.error("Nebylo mozne najit existenci tridy '{}'.",
					className.toString());
		}

		return t;
	}

}
