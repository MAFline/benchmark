package test.suite;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;
import test.mongo.MongoTest;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;

/**
 * This class manages connection to MongoDB database.
 * 
 * @author Lukáš Masařík
 * 
 */
public class MongoTestSuite extends TestSuite<MongoTest, DB> {
	private String host;
	private int port;
	private String dbName;
	private Logger logger;
	private MongoClient mongoClient;

	public MongoTestSuite(String host, String dbName, int port) {
		super();
		this.host = host;
		this.dbName = dbName;
		this.port = port;
		logger = LoggerFactory.getLogger(MongoTestSuite.class);
	}

	@Override
	public void init() {
		logger.info("Inicializace MongoDB testu.");
		connect();

		if (!isConnected()) {
			logger.error("Nepodarilo se pripojit k MongoDB serveru.");
			return;
		}

		logger.info("Pripojeni k MongoDB serveru probehlo uspesne.");

		dbConnection = mongoClient.getDB(dbName);
	}

	/**
	 * create MongoClient to communicate with MongoDB server.
	 */
	private void connect() {
		try {
			mongoClient = new MongoClient(host, port);
		} catch (UnknownHostException | MongoException e) {
			return;
		}
	}

	@Override
	public void addTest(Test test) {
		if (test instanceof MongoTest) {
			tests.add((MongoTest) test);
		} else {
			logger.error("Test '{}' nelze pouzit pro '{}'.", test.getClass()
					.getSimpleName(), this.getClass().getSimpleName());
		}
	}

	@Override
	public boolean isConnected() {
		try {
			mongoClient.getAddress();
		} catch (MongoTimeoutException e) {
			return false;
		}
		return true;
	}

	@Override
	public void tearDown() {
		if (isConnected()) {
			logger.info("Vymazavam databazi '{}' pro MongoDB testy.", dbName);

			try {
				mongoClient.dropDatabase(dbName);
				mongoClient.close();
			} catch (MongoException e) {
				logger.error("Nepodarilo se smazat MongoDB databazi '{}'.",
						dbName);
			}
		}
	}

}
