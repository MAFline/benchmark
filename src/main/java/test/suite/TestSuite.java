package test.suite;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;
import test.time.TestTime;
import test.time.TestTimes;

/**
 * 
 * Abstract class defining basic operations on database. Makes connection and
 * run tests.
 * 
 * @author Lukáš Masařík
 * 
 * @param <T>
 *            Class implements interface Test.
 * @param <T2>
 *            Object communicating with database.
 */
public abstract class TestSuite<T extends Test, T2> {
	protected List<T> tests;
	protected T2 dbConnection;
	private Logger logger;
	private TestTimes testTimes;
	private DecimalFormat df;

	public TestSuite() {
		tests = new ArrayList<>();
		logger = LoggerFactory.getLogger(TestSuite.class);
		dbConnection = null;
		testTimes = new TestTimes();
		df = new DecimalFormat();
		df.setMaximumFractionDigits(3);
	}

	/**
	 * Add Test to suit.
	 * 
	 * @param test
	 */
	public abstract void addTest(Test test);

	/**
	 * Set connection to chosen database system.
	 */
	public abstract void init();

	/**
	 * Return true if database is ready to use.
	 */
	public abstract boolean isConnected();

	/**
	 * Close connection and clear the database after tests.
	 */
	public abstract void tearDown();

	/**
	 * This method runs every test in Suite.
	 */
	public void run() {
		String testSuite = this.getClass().getSimpleName();

		if (isConnected()) {
			logger.info("Spoustim " + testSuite + " testy.");

			for (Test test : tests) {
				if (isConnected()) {
					String testName = test.getClass().getSimpleName();
					TestTime testTime = new TestTime(testName,
							test.getNumOfRecords());

					logger.info("Zahajuji test '" + testName + "' pro "
							+ test.getNumOfRecords() + " zaznamu.");

					test.setConnection(dbConnection);
					test.init();

					testTime.start();
					test.run();
					testTime.stop();
					testTime.getSummaryMessage(1);

					test.tearDown();
					testTimes.add(testTime);
				}
			}

			logger.info("Celkovy cas pro testy '" + testSuite + "' byl "
					+ df.format(testTimes.getTotalTimeInSeconds()) + " s.");
		} else {
			logger.error("Neexistujici pripojeni k databazi pro testy '"
					+ testSuite + "'.");
		}
	}

	public List<T> getTests() {
		return tests;
	}

	public void setTests(List<T> tests) {
		this.tests = tests;
	}

	public T2 getDbConnection() {
		return dbConnection;
	}

	public void setDbConnection(T2 dbConnection) {
		this.dbConnection = dbConnection;
	}

	public TestTimes getTestTimes() {
		return testTimes;
	}

	public void setTestTimes(TestTimes testTimes) {
		this.testTimes = testTimes;
	}

}
