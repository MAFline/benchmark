package test.suite;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;
import test.derby.DerbyTest;

/**
 * This class manages connection to Derby database.
 * 
 * @author Lukáš Masařík
 * 
 */
public class DerbyTestSuite extends TestSuite<DerbyTest, Connection> {
	private String host;
	private int port;
	private String dbName;
	private Logger logger;
	private static final String DB_PATH = "DerbyTest";
	private String derbyFolder;

	public DerbyTestSuite(String host, String dbName, int port) {
		super();
		this.host = host;
		this.dbName = dbName;
		this.port = port;
		logger = LoggerFactory.getLogger(DerbyTestSuite.class);
		Properties p = System.getProperties();
		derbyFolder = p.getProperty("user.home")
				+ p.getProperty("file.separator") + DB_PATH;
	}

	@Override
	public void addTest(Test test) {
		if (test instanceof DerbyTest) {
			tests.add((DerbyTest) test);
		} else {
			logger.error("Test '{}' nelze pouzit pro '{}'.", test.getClass()
					.getSimpleName(), this.getClass().getSimpleName());
		}
	}

	@Override
	public void init() {
		logger.info("Inicializace Derby testu.");

		String driver = "org.apache.derby.jdbc.ClientDriver";

		try {
			Class.forName(driver).newInstance();
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException e) {
			logger.error("Nenalezen driver k Derby databazi.");
			return;
		}

		File f = new File(derbyFolder);
		try {
			f.mkdir();
		} catch (SecurityException e) {
			logger.error("Nemate prava pro cteni/zapis do domovskeho adresare");
			return;
		}

		String url = "jdbc:derby://" + host + ":" + port + "/" + derbyFolder.replace("\\", "/")
				+ "/" + dbName + ";create=true";
		connect(url);

		if (!isConnected()) {
			logger.error("Nepodarilo se pripojit k Derby serveru.");
			return;
		}
	}

	/**
	 * Create connection to Derby.
	 * 
	 * @param url
	 *            Url address to database server.
	 */
	private void connect(String url) {
		try {
			dbConnection = DriverManager.getConnection(url);
		} catch (SQLException e) {
			logger.error("Spojeni s Derby serverem nebylo navazano.");
		}
	}

	@Override
	public boolean isConnected() {
		if (dbConnection == null) {
			return false;
		}
		return true;
	}

	@Override
	public void tearDown() {
		if (isConnected()) {
			logger.info("Vymazavam databazi '{}' pro Derby testy.", dbName);

			try {
				dbConnection.close();
			} catch (SQLException e) {
				logger.error(
						"Nepodarilo se uzavrit spojeni s Derby databazi '{}'.",
						dbName);
			}
		}

		try {
			FileUtils.deleteDirectory(new File(derbyFolder));
		} catch (IOException e) {
			logger.error("Nepodarilo se smazat Derby databazi '{}'.", dbName);
		}

	}

}
