package test.suite;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;
import test.mysql.MysqlTest;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

/**
 * This class manages connection to MySQL database.
 * 
 * @author Lukáš Masařík
 * 
 */
public class MysqlTestSuite extends TestSuite<MysqlTest, Connection> {
	private String host;
	private int port;
	private String dbName;
	private String username;
	private String password;
	private Logger logger;

	public MysqlTestSuite(String host, String dbName, int port,
			String username, String password) {
		super();
		this.host = host;
		this.dbName = dbName;
		this.port = port;
		this.username = username;
		this.password = password;
		logger = LoggerFactory.getLogger(MysqlTestSuite.class);
	}

	@Override
	public void init() {
		logger.info("Inicializace MySQL testu.");
		String url = "jdbc:mysql://" + host + ":" + port;
		connect(url);

		if (!isConnected()) {
			logger.info("Nepodarilo se pripojit k MySQL serveru.");
			return;
		}

		logger.info("Pripojeni k MySQL serveru probehlo uspesne.");

		createDatabase(dbName);

		url += "/" + dbName
				+ "?useServerPrepStmts=false&rewriteBatchedStatements=true";

		connect(url);
	}

	@Override
	public void addTest(Test test) {
		if (test instanceof MysqlTest) {
			tests.add((MysqlTest) test);
		} else {
			logger.error("Test '{}' nelze pouzit pro '{}'.", test.getClass()
					.getSimpleName(), this.getClass().getSimpleName());
		}
	}

	/**
	 * Create connection to MySQL.
	 * 
	 * @param url
	 *            Url address to database server.
	 */
	private void connect(String url) {
		try {
			dbConnection = (Connection) DriverManager.getConnection(url,
					username, password);
		} catch (SQLException e) {
			logger.error("Spojeni s MySQL serverem nebylo navazano.");
		}
	}

	/**
	 * Create new database on MySQL server.
	 * 
	 * @param dbName
	 *            Name of database.
	 */
	private void createDatabase(String dbName) {
		String sqlCreateDatabase = "CREATE DATABASE IF NOT EXISTS " + dbName;
		try (PreparedStatement query = (PreparedStatement) dbConnection
				.prepareStatement(sqlCreateDatabase)) {
			query.executeUpdate();
			dbConnection.close();
		} catch (SQLException e) {
			logger.error("Nepodarilo se vytvorit databazi '{}'.", dbName);
		}
	}

	@Override
	public boolean isConnected() {
		String create = "CREATE DATABASE IF NOT EXISTS " + dbName;
		try (PreparedStatement checkConnection = (PreparedStatement) dbConnection
				.prepareStatement(create)) {

			checkConnection.executeUpdate();
		} catch (NullPointerException | SQLException e) {
			return false;
		}
		return true;
	}

	@Override
	public void tearDown() {
		if (isConnected()) {
			logger.info("Vymazavam databazi '{}' pro MySQL testy.", dbName);

			try (PreparedStatement dropDatabase = (PreparedStatement) dbConnection
					.prepareStatement("DROP DATABASE " + dbName)) {

				dropDatabase.executeUpdate();
				dbConnection.close();
			} catch (NullPointerException | SQLException e) {
				logger.error("Nepodarilo se smazat MySQL databazi '{}'.",
						dbName);
			}
		}
	}
}
