package test.time;

import java.text.DecimalFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class start and end time to measure tests.
 * 
 * @author Lukáš Masařík
 * 
 */
public class TestTime {
	private String testName;
	private int numOfRecords;
	private double startTime, endTime, totalTime;
	private Logger logger;
	private DecimalFormat df;

	public TestTime(String testName, int numOfRecords) {
		this.testName = testName;
		this.numOfRecords = numOfRecords;
		logger = LoggerFactory.getLogger(TestTime.class);
		df = new DecimalFormat();
		df.setMaximumFractionDigits(3);
	}

	/**
	 * Starting timekeeping.
	 */
	public void start() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Stop timekeeping.
	 */
	public void stop() {
		endTime = System.currentTimeMillis();
		totalTime = endTime - startTime;
	}

	/**
	 * Method to compute total time of test per record.
	 * 
	 * @return Time of tests per record.
	 */
	public double getTimePerRecord() {
		return totalTime / numOfRecords;
	}

	/**
	 * Logger message with result of TestSuite
	 * 
	 * @param option
	 *            indicator of message 1 for whole message (time, records, time
	 *            per record) 2 only for time in seconds.
	 */
	public void getSummaryMessage(int option) {
		switch (option) {
		case 1:
			logger.info(
					"Celkovy cas pro '{}': {} sekund pro {} zaznamu ({} ms/zaznam).",
					testName, df.format(totalTime / 1000), numOfRecords,
					df.format(getTimePerRecord()));
			break;
		case 2:
			logger.info(df.format(totalTime / 1000));
			break;
		default:
			logger.info(
					"Celkovy cas pro '{}': {} sekund pro {} zaznamu ({} ms/zaznam).",
					testName, df.format(totalTime / 1000), numOfRecords,
					df.format(getTimePerRecord()));
			break;
		}

	}

	public double getTotalTime() {
		return totalTime;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public int getNumOfRecords() {
		return numOfRecords;
	}

	public void setNumOfRecords(int numOfRecords) {
		this.numOfRecords = numOfRecords;
	}

}
