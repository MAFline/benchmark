package test.time;

import java.util.ArrayList;
import java.util.List;

/**
 * Class using collection of TestTime to summarize time of all test under the
 * TestSuite.
 * 
 * @author Lukáš Masařík
 * 
 */
public class TestTimes {
	private List<TestTime> testTimes;

	public TestTimes() {
		testTimes = new ArrayList<>();
	}

	/**
	 * Add TestTime to collection.
	 * 
	 * @param tT
	 *            TestTime of one Test.
	 */
	public void add(TestTime tT) {
		testTimes.add(tT);
	}

	/**
	 * Return whole time of all Tests under one database.
	 * 
	 * @return Whole time of all Tests in milliseconds.
	 */
	public double getTotalTime() {
		double totalTime = 0;
		for (TestTime tT : testTimes) {
			totalTime += tT.getTotalTime();
		}

		return totalTime;
	}

	/**
	 * Return whole time of all Tests under one database in seconds.
	 * 
	 * @return Whole time of all Tests in seconds.
	 */
	public double getTotalTimeInSeconds() {
		return getTotalTime() / 1000;
	}

}
