package test.derby.select;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.derby.DerbyTest;

public class DerbySelectValuesByQueryWithIndex extends DerbyTest {
	private PreparedStatement psSelect;
	private Logger logger;

	public DerbySelectValuesByQueryWithIndex(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory
				.getLogger(DerbySelectValuesByQueryWithIndex.class);
	}

	@Override
	public void init() {
		insertPersons(numOfRecords, true);

		String sqlSelect = "SELECT firstName, lastName, salary, city FROM "
				+ tables.get("persons") + " JOIN " + tables.get("addresses")
				+ " using (id_address)"
				+ " WHERE city = 'Brno' AND salary >= 40000";

		try {
			psSelect = connection.prepareStatement(sqlSelect);
		} catch (SQLException e) {
			logger.error("Nepodarilo se iniciovat Derby prikaz pro cteni.");
		}
	}

	@Override
	public void run() {
		try (ResultSet rs = psSelect.executeQuery()) {
			psSelect.close();
		} catch (SQLException e) {
			logger.error("Cteni se nezdarilo");
		}
	}

	@Override
	public void tearDown() {
		String sql = "DROP TABLE ";

		try (PreparedStatement persons = connection.prepareStatement(sql
				+ tables.get("persons"));
				PreparedStatement addresses = connection.prepareStatement(sql
						+ tables.get("addresses"))) {

			persons.executeUpdate();
			addresses.executeUpdate();
			
			tables.remove("persons");
			tables.remove("addresses");
		} catch (SQLException e) {
			logger.error("Nelze smazat tabulky po ukonceni testu.");
		}
	}

}
