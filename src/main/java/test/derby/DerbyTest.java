package test.derby;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;
import data.Address;
import data.DataFactory;
import data.Person;

/**
 * Class implements interface Test and collect common parameters and methods for
 * Derby tests.
 * 
 * @author Lukáš Masařík
 * 
 */
public abstract class DerbyTest implements Test {
	protected Connection connection;
	protected int numOfRecords;
	protected Map<String, String> tables;
	protected List<PreparedStatement> pstmsAddresses;
	protected List<PreparedStatement> pstmsPersons;
	private DateFormat df;
	private Logger logger;

	public DerbyTest(int numOfRecords) {
		this.numOfRecords = numOfRecords;
		df = new SimpleDateFormat("ddMMyyyy_HHmmss");
		tables = new HashMap<>();
		logger = LoggerFactory.getLogger(DerbyTest.class);
	}

	/**
	 * Create Derby table, add suffix with class and actual date.
	 * 
	 * @param name
	 *            Table name
	 */
	protected void createTable(String name) {
		String tableName = name
				+ "_"
				+ this.getClass().getSimpleName().toLowerCase()
						.replace(".", "_") + "_" + df.format(new Date());

		String sqlCreateTable = "CREATE TABLE " + tableName
				+ getTableColumns(name);

		try (PreparedStatement createTable = connection
				.prepareStatement(sqlCreateTable);) {

			createTable.executeUpdate();
			tables.put(name, tableName);

			Thread.sleep(1000);
		} catch (SQLException e) {
			logger.error("Nepodarilo se vytvorit tabulku '{}'.", tableName, e);
		} catch (InterruptedException e) {
			logger.error("Chyba uspani vlakna.");
		}
	}

	/**
	 * Generate String to CREATE TABLE command.
	 * 
	 * @param table
	 *            Table which will be created. Possible values: addresses,
	 *            persons.
	 * @return String with parameters to create new table.
	 */
	private String getTableColumns(String table) {
		String sqlColumns = new String();
		switch (table) {
		case "addresses":
			sqlColumns = " (id_address VARCHAR(50) NOT NULL, street VARCHAR(50), houseNumber INTEGER, city VARCHAR(50), PRIMARY KEY (id_address))";
			break;
		case "persons":
			sqlColumns = " (id_person VARCHAR(50) NOT NULL, firstName VARCHAR(20), lastName VARCHAR(50), age INTEGER, salary INTEGER, id_address VARCHAR(50), PRIMARY KEY (id_person))";
			break;
		default:
			break;
		}
		return sqlColumns;
	}

	/**
	 * Set foreign key constraint between two tables.
	 * 
	 * @param table
	 *            table name where you put foreign key constraint.
	 * @param column
	 *            column which references to other table.
	 * @param referencedTable
	 *            table name where foreign key points.
	 * @param referencedColumn
	 *            column on referenced table.
	 */
	protected void setForeignKey(String table, String column,
			String referencedTable, String referencedColumn) {
		String sqlForeignKey = "ALTER TABLE " + tables.get(table)
				+ " ADD FOREIGN KEY (" + column + ") REFERENCES "
				+ tables.get(referencedTable) + "(" + referencedColumn + ")";

		try (PreparedStatement foreignKey = connection
				.prepareStatement(sqlForeignKey)) {
			foreignKey.executeUpdate();
		} catch (SQLException e) {
			logger.error(
					"Nepodarilo se vytvorit cizi klic mezi tabulkami '{}' a '{}'.",
					tables.get(table), tables.get(referencedTable));
		}
	}

	/**
	 * Create index on selected table and column.
	 * 
	 * @param table
	 *            Table where to create index.
	 * @param column
	 *            Column where ensure index.
	 */
	protected void createIndex(String table, String column) {
		String sqlCreateIndex = "CREATE INDEX index_" + column + "_"
				+ tables.get(table) + " ON " + tables.get(table) + " ("
				+ column + ")";

		try (PreparedStatement createIndex = connection
				.prepareStatement(sqlCreateIndex)) {
			createIndex.executeUpdate();
		} catch (SQLException e) {
			logger.error(
					"Nepodarilo se vytvorit index na sloupci '{}' tabulky '{}'.",
					column, tables.get(table));
		}
	}

	/**
	 * Insert List of persons to the database. The method will create two tables
	 * - Addresses and Persons. Then will insert the list.
	 * 
	 * @param count
	 *            - number of records to insert.
	 * @param index
	 *            - create table with or without index
	 */
	protected void insertPersons(int count, boolean index) {
		createTable("persons");
		createTable("addresses");
		setForeignKey("persons", "id_address", "addresses", "id_address");

		List<Person> persons = DataFactory.generatePersons(numOfRecords);

		createPreparedStatementsPersons(persons);

		try {

			for (PreparedStatement ps : pstmsAddresses) {
				ps.executeBatch();
				ps.close();
			}

			for (PreparedStatement ps : pstmsPersons) {
				ps.executeBatch();
				ps.close();
			}

			if (index) {
				createIndex("addresses", "city");
				createIndex("persons", "lastName");
				createIndex("persons", "salary");
			}
		} catch (SQLException e) {
			logger.error("Zapis se nezdaril.", e);
		}
	}

	/**
	 * Max size of Derby batch is 65k. So this method is for distribution
	 * PreparedStatements and fill in the collection List pstmsPersons.
	 * 
	 * @param persons
	 *            List of Persons.
	 */
	protected void createPreparedStatementsPersons(List<Person> persons) {
		pstmsPersons = new ArrayList<>();

		List<Address> addresses = new ArrayList<>();

		for (Person person : persons) {
			addresses.add(person.getAddress());
		}

		createPreparedStatementsAddresses(addresses);

		String sqlInsertPerson = "INSERT INTO "
				+ tables.get("persons")
				+ " (id_person, firstName, lastName, age, salary, id_address) VALUES (?, ?, ?, ?, ?, ?)";

		int delimiter = 60000;
		int length = persons.size();
		int groups = length / delimiter;

		try {

			for (int i = 0; i < groups; i++) {

				PreparedStatement ps = connection
						.prepareStatement(sqlInsertPerson);

				for (int j = i * delimiter; j < (i + 1) * delimiter; j++) {
					ps.setString(1, persons.get(j).getId());
					ps.setString(2, persons.get(j).getFirstName());
					ps.setString(3, persons.get(j).getLastName());
					ps.setInt(4, persons.get(j).getAge());
					ps.setInt(5, persons.get(j).getSalary());
					ps.setString(6, persons.get(j).getAddress().getId());

					ps.addBatch();
				}

				pstmsPersons.add(ps);
			}

			if (length != delimiter * groups) {

				PreparedStatement ps = connection
						.prepareStatement(sqlInsertPerson);

				for (int i = groups * delimiter; i < length; i++) {
					ps.setString(1, persons.get(i).getId());
					ps.setString(2, persons.get(i).getFirstName());
					ps.setString(3, persons.get(i).getLastName());
					ps.setInt(4, persons.get(i).getAge());
					ps.setInt(5, persons.get(i).getSalary());
					ps.setString(6, persons.get(i).getAddress().getId());

					ps.addBatch();
				}

				pstmsPersons.add(ps);
			}

		} catch (SQLException e) {
			logger.error("Nepodarilo se vytvorit prikazy pro Derby.", e);
		}

	}

	/**
	 * Max size of Derby batch is 65k. So this method is for distribution
	 * PreparedStatements and fill in the collection List.
	 * 
	 * @param addresses
	 *            List of Addresses.
	 */
	protected void createPreparedStatementsAddresses(List<Address> addresses) {
		pstmsAddresses = new ArrayList<>();

		String sqlInsertAddress = "INSERT INTO "
				+ tables.get("addresses")
				+ " (id_address, street, houseNumber, city) VALUES (?, ?, ?, ?)";

		int delimiter = 60000;
		int length = addresses.size();
		int groups = length / delimiter;

		try {

			for (int i = 0; i < groups; i++) {

				PreparedStatement ps = connection
						.prepareStatement(sqlInsertAddress);

				for (int j = i * delimiter; j < (i + 1) * delimiter; j++) {
					ps.setString(1, addresses.get(j).getId());
					ps.setString(2, addresses.get(j).getStreet());
					ps.setInt(3, addresses.get(j).getHouseNumber());
					ps.setString(4, addresses.get(j).getCity());

					ps.addBatch();
				}

				pstmsAddresses.add(ps);
			}

			if (length != delimiter * groups) {

				PreparedStatement ps = connection
						.prepareStatement(sqlInsertAddress);

				for (int i = groups * delimiter; i < length; i++) {
					ps.setString(1, addresses.get(i).getId());
					ps.setString(2, addresses.get(i).getStreet());
					ps.setInt(3, addresses.get(i).getHouseNumber());
					ps.setString(4, addresses.get(i).getCity());

					ps.addBatch();
				}

				pstmsAddresses.add(ps);
			}

		} catch (SQLException e) {
			logger.error("Nepodarilo se vytvorit prikazy pro Derby.", e);
		}

	}

	@Override
	public void setConnection(Object connection) {
		if (connection == null) {
			throw new RuntimeException(
					"Nelze komunikovat s databází (nenalezen objekt ke komunikaci)");
		}
		this.connection = (Connection) connection;
	}

	@Override
	public int getNumOfRecords() {
		return numOfRecords;
	}

}
