package test.derby.insert;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.derby.DerbyTest;
import data.DataFactory;
import data.Person;

public class DerbyInsertBatch extends DerbyTest {
	private List<Person> persons;
	private Logger logger;

	public DerbyInsertBatch(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(DerbyInsertBatch.class);
	}

	@Override
	public void init() {
		createTable("persons");
		createTable("addresses");
		setForeignKey("persons", "id_address", "addresses", "id_address");

		persons = DataFactory.generatePersons(numOfRecords);
		
		createPreparedStatementsPersons(persons);
	}

	@Override
	public void run() {
		
		try {
			
			for (PreparedStatement ps : pstmsAddresses) {
				ps.executeBatch();
				ps.close();
			}

			for (PreparedStatement ps : pstmsPersons) {
				ps.executeBatch();
				ps.close();
			}
		} catch (SQLException e) {
			logger.error("Zapis se nezdaril.", e);
		}
	}

	@Override
	public void tearDown() {
		String sql = "DROP TABLE ";
		
		try (PreparedStatement persons = connection.prepareStatement(sql
				+ tables.get("persons"));
				PreparedStatement addresses = connection.prepareStatement(sql
						+ tables.get("addresses"))) {
			
			persons.executeUpdate();
			addresses.executeUpdate();
			
			tables.remove("persons");
			tables.remove("addresses");
		} catch (SQLException e) {
			logger.error("Nelze smazat tabulky po ukonceni testu.");
		}
	}

}
