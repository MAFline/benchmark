package test.derby.insert;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.derby.DerbyTest;
import data.Address;
import data.DataFactory;

public class DerbyInsertIndividualyAddresses extends DerbyTest {
	private List<Address> addresses;
	private PreparedStatement psAddress;
	private Logger logger;

	public DerbyInsertIndividualyAddresses(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(DerbyInsertIndividualyAddresses.class);
	}

	@Override
	public void init() {
		createTable("addresses");

		String sqlInsertAddress = "INSERT INTO "
				+ tables.get("addresses")
				+ " (id_address, street, houseNumber, city) VALUES (?, ?, ?, ?)";

		addresses = DataFactory.generateAddresses(numOfRecords);

		try {
			psAddress = connection
					.prepareStatement(sqlInsertAddress);
		} catch (SQLException e) {
			logger.error("Nepodarilo se iniciovat Derby prikaz pro zapis.");
		}
	}

	@Override
	public void run() {
		try {
			for (Address a : addresses) {
				psAddress.setString(1, a.getId());
				psAddress.setString(2, a.getStreet());
				psAddress.setInt(3, a.getHouseNumber());
				psAddress.setString(4, a.getCity());

				psAddress.executeUpdate();
			}
			psAddress.close();
		} catch (SQLException e) {
			logger.error("Zapis se nezdaril");
		}
	}

	@Override
	public void tearDown() {
		String sql = "DROP TABLE ";

		try (PreparedStatement addresses = connection.prepareStatement(sql
				+ tables.get("addresses"))) {

			addresses.executeUpdate();
			
			tables.remove("addresses");
		} catch (SQLException e) {
			logger.error("Nelze smazat tabulku po ukonceni testu.");
		}
	}

}
