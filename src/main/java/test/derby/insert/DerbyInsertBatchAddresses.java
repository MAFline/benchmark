package test.derby.insert;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.derby.DerbyTest;
import data.Address;
import data.DataFactory;

public class DerbyInsertBatchAddresses extends DerbyTest {
	private List<Address> addresses;
	private Logger logger;

	public DerbyInsertBatchAddresses(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(DerbyInsertBatchAddresses.class);
	}

	@Override
	public void init() {
		createTable("addresses");

		addresses = DataFactory.generateAddresses(numOfRecords);

		createPreparedStatementsAddresses(addresses);
	}

	@Override
	public void run() {
		try {
			for (PreparedStatement ps : pstmsAddresses) {
				ps.executeBatch();
				ps.close();
			}
		} catch (SQLException e) {
			logger.error("Zapis se nezdaril", e);
		}
	}

	@Override
	public void tearDown() {
		String sql = "DROP TABLE ";

		try (PreparedStatement addresses = connection.prepareStatement(sql
				+ tables.get("addresses"))) {

			addresses.executeUpdate();
			
			tables.remove("addresses");
		} catch (SQLException e) {
			logger.error("Nelze smazat tabulku po ukonceni testu.");
		}
	}

}
