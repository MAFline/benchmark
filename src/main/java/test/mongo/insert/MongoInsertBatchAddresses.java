package test.mongo.insert;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.mongo.MongoTest;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

import data.DataFactory;

public class MongoInsertBatchAddresses extends MongoTest {
	private List<DBObject> addresses;
	private DBCollection collection;
	private Logger logger;

	public MongoInsertBatchAddresses(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(MongoInsertBatchAddresses.class);
	}

	@Override
	public void init() {
		collection = createCollection("addresses");
		addresses = generateAddressesDocuments(DataFactory
				.generateAddresses(numOfRecords));
	}

	@Override
	public void run() {
		try {
			collection.insert(addresses);
		} catch (MongoException e) {
			logger.error("Zapis se nezdaril");
		}
	}
	
	@Override
	public void tearDown() {
		try {
			collection.drop();
			collections.remove("addresses");
		} catch (MongoException e) {
			logger.error("Nelze dropnout kolekci '{}'.", collection.getName());
		}
	}

}
