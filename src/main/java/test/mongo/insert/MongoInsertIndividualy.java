package test.mongo.insert;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.mongo.MongoTest;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

import data.DataFactory;

public class MongoInsertIndividualy extends MongoTest {
	private List<DBObject> persons;
	private DBCollection collection;
	private Logger logger;

	public MongoInsertIndividualy(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(MongoInsertIndividualy.class);
	}

	@Override
	public void init() {
		collection = createCollection("persons");
		persons = generatePersonsDocuments(DataFactory
				.generatePersons(numOfRecords));
	}

	@Override
	public void run() {
		try {
			for (DBObject person : persons) {
				collection.insert(person);
			}
		} catch (MongoException e) {
			logger.error("Zapis se nezdaril");
		}
	}
	
	@Override
	public void tearDown() {
		try {
			collection.drop();
			collections.remove("persons");
		} catch (MongoException e) {
			logger.error("Nelze dropnout kolekci '{}'.", collection.getName());
		}
	}

}
