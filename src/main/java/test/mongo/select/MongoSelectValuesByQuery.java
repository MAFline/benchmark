package test.mongo.select;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.mongo.MongoTest;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

import data.DataFactory;

public class MongoSelectValuesByQuery extends MongoTest {
	private DBCollection collection;
	private DBObject criteria, projection;
	private Logger logger;

	public MongoSelectValuesByQuery(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(MongoSelectValuesByQuery.class);
	}

	@Override
	public void init() {
		collection = createCollection("persons");

		criteria = new BasicDBObject("address.city", "Brno").append("salary",
				new BasicDBObject("$gte", 40000));
		projection = new BasicDBObject("_id", 0).append("firstName", 1)
				.append("lastName", 1).append("salary", 1)
				.append("address.city", 1);

		try {
			collection.insert(generatePersonsDocuments(DataFactory
					.generatePersons(numOfRecords)));
		} catch (MongoException e) {
			logger.error("Nepodarilo se iniciovat MongoDB kolekci pro cteni.");
		}

	}

	@Override
	public void run() {
		DBCursor cursor = collection.find(criteria, projection);
		cursor.close();
	}
	
	@Override
	public void tearDown() {
		try {
			collection.drop();
			collections.remove("persons");
		} catch (MongoException e) {
			logger.error("Nelze dropnout kolekci '{}'.", collection.getName());
		}
	}

}
