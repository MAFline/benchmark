package test.mongo.select;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.mongo.MongoTest;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

import data.DataFactory;

public class MongoSelectFullByQuery extends MongoTest {
	private DBCollection collection;
	private DBObject criteria;
	private Logger logger;

	public MongoSelectFullByQuery(int numOfRecords) {
		super(numOfRecords);
		logger = LoggerFactory.getLogger(MongoSelectFullByQuery.class);
	}

	@Override
	public void init() {
		collection = createCollection("persons");

		criteria = new BasicDBObject("address.city", "Brno").append("salary",
				new BasicDBObject("$gte", 40000));

		try {
			collection.insert(generatePersonsDocuments(DataFactory
					.generatePersons(numOfRecords)));
		} catch (MongoException e) {
			logger.error("Nepodarilo se iniciovat MongoDB kolekci pro cteni.");
		}

	}

	@Override
	public void run() {
		DBCursor cursor = collection.find(criteria);
		cursor.close();
	}
	
	@Override
	public void tearDown() {
		try {
			collection.drop();
			collections.remove("persons");
		} catch (MongoException e) {
			logger.error("Nelze dropnout kolekci '{}'.", collection.getName());
		}
	}

}
