package test.mongo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

import data.Address;
import data.Person;

/**
 * Class implements interface Test and collect common parameters and methods for
 * MongoDB tests.
 * 
 * @author Lukáš Masařík
 *
 */
public abstract class MongoTest implements Test {
	protected DB db;
	protected int numOfRecords;
	protected Map<String, String> collections;
	private DateFormat df;
	private Logger logger;

	public MongoTest(int numOfRecords) {
		this.numOfRecords = numOfRecords;
		df = new SimpleDateFormat("ddMMyyyy_HHmmss");
		collections = new HashMap<>();
		logger = LoggerFactory.getLogger(MongoTest.class);
	}

	/**
	 * Create MongoDB collection with chosen name, add suffix with class and
	 * actual date.
	 * 
	 * @param name
	 *            name of collection.
	 * @return new MongoDB collection.
	 */
	protected DBCollection createCollection(String name) {
		String collectionName = name + "_"
				+ this.getClass().getSimpleName().toLowerCase() + "_"
				+ df.format(new Date());
		DBCollection collection = null;

		try {
			collection = db.createCollection(collectionName, null);
			collections.put(name, collectionName);
			Thread.sleep(1000);
		} catch (MongoException e) {
			logger.error("Nepodarilo se vytvorit kolekci '{}'.", collectionName);
		} catch (InterruptedException e) {
			logger.error("Chyba uspani vlakna.");
		}
		return collection;
	}

	/**
	 * Convert List of Persons to List of DBObjects.
	 * 
	 * @param persons
	 *            List of Persons.
	 * @return List of DBObjects.
	 */
	protected List<DBObject> generatePersonsDocuments(List<Person> persons) {
		List<DBObject> personsDocs = new ArrayList<>();
		for (Person person : persons) {
			DBObject personObj = new BasicDBObject("_id", person.getId())
					.append("firstName", person.getFirstName())
					.append("lastName", person.getLastName())
					.append("age", person.getAge())
					.append("salary", person.getSalary())
					.append("address",
							new BasicDBObject("street", person.getAddress()
									.getStreet()).append("houseNumber",
									person.getAddress().getHouseNumber())
									.append("city",
											person.getAddress().getCity()));
			personsDocs.add(personObj);
		}
		return personsDocs;
	}

	/**
	 * Convert List of Addresses to List of DBObjects.
	 * 
	 * @param addresses
	 *            List of Addresses.
	 * @return List of DBObjects.
	 */
	protected List<DBObject> generateAddressesDocuments(List<Address> addresses) {
		List<DBObject> addressesDocs = new ArrayList<>();
		for (Address address : addresses) {
			DBObject addressObj = new BasicDBObject("_id", address.getId())
					.append("street", address.getStreet())
					.append("houseNumber", address.getHouseNumber())
					.append("city", address.getCity());
			addressesDocs.add(addressObj);
		}
		return addressesDocs;
	}

	@Override
	public void setConnection(Object connection) {
		if (connection == null) {
			throw new RuntimeException(
					"Nelze komunikovat s databází (nenalezen objekt ke komunikaci)");
		}
		db = (DB) connection;
	}

	@Override
	public int getNumOfRecords() {
		return numOfRecords;
	}

}
